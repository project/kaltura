<?php

/**
 * @file
 * Kaltura media upload functions.
 */

/**
 * Form constructor to upload media files to Kaltura.
 *
 * @see kaltura_uploader_form_submit()
 * @ingroup forms
 */
function kaltura_uploader_form($form, &$form_state) {
  // Do not specify the #upload_location to not move the file from the temporary
  // directory.
  $form['media'] = array(
    '#title' => t('File'),
    '#title_display' => 'invisible',
    '#type' => 'managed_file',
    '#required' => TRUE,
    '#progress_indicator' => 'bar',
    '#upload_validators' => array(
      // Set this validator but with no extensions in order to accept any.
      'file_validate_extensions' => array(),
      'kaltura_uploader_validate_file' => array(),
    ),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Upload'),
  );

  return $form;
}

/**
 * Form submission handler for kaltura_uploader_form().
 */
function kaltura_uploader_form_submit($form, &$form_state) {
  $file = file_load($form_state['values']['media']);

  try {
    $helpers = new KalturaHelpers();
    $client = $helpers->getKalturaClient();

    $entry = new KalturaMediaEntry();
    $entry->name = pathinfo($file->filename, PATHINFO_FILENAME);
    $entry->mediaType = kaltura_get_media_type_by_mime($file->filemime);

    $token = $client->media->upload(drupal_realpath($file->uri));
    $entry = $client->media->addFromUploadedFile($entry, $token);

    $entity = kaltura_entry_create(array('kaltura_entryid' => $entry->id));
    kaltura_map_properties($entry, $entity);
    // No need to call kaltura_save_entry_metadata() because we've just created
    // the entry and there's no fields filled yet.
    kaltura_entry_save($entity);

    // "Return" the ID to insert it to original field if called from Kaltura
    // widget.
    $form_state['eid'] = $entry->id;
    $form_state['media_type'] = $entry->mediaType;
  }
  catch (Exception $e) {
    watchdog_exception('kaltura', $e);
    drupal_set_message(t('Failed to start Kaltura session. Please check your settings.'), 'error');
  }
}

/**
 * Upload validator: Checks that the file type is allowed by Kaltura.
 *
 * @param object $file
 *   A Drupal file object.
 *
 * @return string[]
 *   An array. If the file is not allowed, it will contain an error message.
 */
function kaltura_uploader_validate_file($file) {
  $errors = array();

  if (!kaltura_get_media_type_by_mime($file->filemime)) {
    $errors[] = t('Only images, audio and video files are allowed.');
  }

  return $errors;
}

/**
 * Given the file mime type returns the Kaltura media type.
 *
 * @param string $mime
 *   Mime type.
 *
 * @return int|null
 *   Kaltura media type or NULL if type is not supported.
 */
function kaltura_get_media_type_by_mime($mime) {
  list($type) = explode('/', $mime);

  switch ($type) {
    case 'image':
      return KALTURA_MEDIA_TYPE_IMAGE;

    case 'video':
      return KALTURA_MEDIA_TYPE_VIDEO;

    case 'audio':
      return KALTURA_MEDIA_TYPE_AUDIO;

    default:
      return NULL;
  }
}
